SELECT GroupId, COUNT(*) FROM Message, GroupMessage
JOIN Session On Date > Session.LastLoginDate
  WHERE Session.Id = 123321
WHERE MessageId = Message.id AND GroupId IN (
  SELECT GroupId FROM Groups
  WHERE userId = 123321
)
GROUP BY GroupId
UNION
SELECT ChannelId, COUNT(*) FROM Message, ChannelMessage
JOIN Session On Date > Session.LastLoginDate
  WHERE Session.Id = 123321
WHERE MessageId = Message.id AND ChannelId IN (
  SELECT ChannelId FROM Channels
  WHERE userId = 123321
)
GROUP BY ChannelId
UNION
SELECT SenderId, COUNT(*) FROM Message
JOIN Session On Date > Session.LastLoginDate
  WHERE Session.Id = 123321
GROUP BY SenderID