SELECT Distinct Channel.Name, Group.Name, Account.Name FROM Message, Channel, Group, Account
JOIN Session On Session.LastLoginDate < Date
WHERE SenderId = Account.Id OR Message.Id In (
  SELECT GroupMessage.MessageId FROM GroupMessage, Groups
  WHERE Groups.userId = 123321 AND Groups.GroupId = GroupMessage.GroupId
) OR Message.Id In (
  SELECT ChannelMessage.MessageId FROM ChannelMessage, Channels
  WHERE Channels.userId = 123321 AND Channels.ChannelId = ChannelMessage.ChannelId
)