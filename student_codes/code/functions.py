from datetime import datetime
import time

import db
import elasticsearch

last_user_id = None


def filter_object(model, values, projects):
    projects_format = ', '.join(projects)
    values_format = ' AND '.join(['%s  = %%s' % k for k in values])

    conn = db.get_conn()
    cur = conn.cursor()

    select = 'SELECT %s FROM %s WHERE %s' % (projects_format, model,
                                             values_format)
    cur.execute(select, [v for k, v in values.items()])
    result = cur.fetchall()
    conn.commit()
    cur.close()
    return result


def get_object(model, values, projects):
    objects = filter_object(model, values, projects)
    if len(objects) == 0:
        return None
    if len(objects) == 1:
        return objects[0]
    raise Exception("Too many objects: %s" % model)


def create_object(model, values):
    conn = db.get_conn()
    cur = conn.cursor()

    names = ', '.join([k for k in values])
    values_format = ', '.join(['%s'] * len(values))
    select = 'INSERT INTO %s (%s) VALUES (%s)' % (model, names, values_format)
    cur.execute(select, [v for k, v in values.items()])

    conn.commit()
    cur.close()

    if model == 'Message':
        elasticsearch.create_object(values)
        time.sleep(1)


def update_object(model, values, new_vals):
    # potential to have BUGS!
    if model == 'Message':
        raise Exception("can't change message")

    objects = filter_object(model, values, '*')
    if not objects:
        raise Exception('No Such Object.')

    conn = db.get_conn()
    cur = conn.cursor()

    values_format = ' AND '.join(['%s  = %%s' % k for k in values])
    new_vals_format = ', '.join(['%s  = %%s' % k for k in new_vals])
    sql = 'UPDATE %s SET %s WHERE %s' % (model, new_vals_format, values_format)
    cur.execute(sql, [v for k, v in new_vals.items()] + [v for k, v in
                                                         values.items()])

    conn.commit()
    cur.close()


def delete_object(model, values):
    if model == 'Message':
        raise Exception("can't delete message")

    conn = db.get_conn()
    cur = conn.cursor()
    values_format = ' AND '.join(['%s  = %%s' % k for k in values])
    sql = 'DELETE FROM %s WHERE %s' % (model, values_format)
    cur.execute(sql, [v for k, v in values.items()])

    conn.commit()
    cur.close()
    pass


def login(phone):
    global last_user_id
    if last_user_id is not None:
        raise Exception("User has been logged in")

    account = get_object('Account', {'PhoneNumber': phone}, ['Id'])
    if account is None:
        print("ID is not found")
        return
    account_id = account[0]
    setting = get_object('AccountSetting', {'AccountId': account_id},
                         ['Password'])
    if setting is None:
        raise Exception("Account without setting")
    password = setting[0]

    if password is not None:
        print("Enter your password :")
        user_password = input()
        if user_password != password:
            print("Access Denied")
            return

    session = get_object('Session', {'AccountId': account_id}, ['Id'])
    if session is not None:
        update_object('Session', {'AccountId': account_id},
                      {'LastLoginDate': datetime.now()})
    else:
        create_object('Session',
                      {'AccountId': account_id, 'CreatedDate': datetime.now(),
                       'LastLoginDate': datetime.now()})

    last_user_id = account_id


def logout():
    global last_user_id
    if last_user_id is None:
        raise Exception("There is no User that has been logged in")
    last_user_id = None


def set_bio(*bio):
    global last_user_id
    bio = ' '.join(bio)
    update_object('AccountSetting', {'AccountId': last_user_id}, {'Bio': bio})


def set_self_destruct(val):
    global last_user_id
    update_object('AccountSetting', {'AccountId': last_user_id},
                  {'SelfDestruct': val})


def set_password(password):
    global last_user_id
    update_object('AccountSetting', {'AccountId': last_user_id},
                  {'Password': password})


def block_user(phone_no):
    global last_user_id
    blocked = get_object('Account', {'PhoneNumber': phone_no}, ['Id'])
    if blocked is None:
        print("ID is not found")
        return

    blocked_id = blocked[0]
    now = datetime.now()
    create_object('BlockedUser', {'BlockedId': blocked_id,
                                  'BlockerId': last_user_id,
                                  'Date': now})


def unblock_user(phone_no):
    global last_user_id
    blocked = get_object('Account', {'PhoneNumber': phone_no}, ['Id'])
    if blocked is None:
        print("ID is not found")
        return

    blocked_id = blocked[0]
    delete_object('BlockedUser', {'BlockedId': blocked_id,
                                  'BlockerId': last_user_id})


def create_user(phone_no):
    create_object('Account', {'PhoneNumber': phone_no}) #, 'Name': phone_no
    account_id = get_object('Account', {'PhoneNumber': phone_no}, ['Id'])[0]
    create_object('AccountSetting', {'AccountId': account_id})


def set_name(name):
    global last_user_id
    update_object('Account', {'Id': last_user_id}, {'Name': name})


def create_conversation_if_necessary(id_, type_, name=None):
    global last_user_id

    conversation = get_object('Conversation', {'Id': id_, 'Type': type_},
                              ['Id'])
    if conversation is not None:
        return

    create_object('Conversation', {'Id': id_, 'Type': type_, 'Name': name})
    if last_user_id is None:
        return
    create_object('Membership', {'AccountId': last_user_id,
                                 'ConversationId': id_,
                                 'ConversationType': type_,
                                 'LastVisit': datetime.now(),
                                 'IsAdmin': True})
    if type_ == 'D':
        arr = id_.split('_')
        opponent = arr[0]
        s = last_user_id
        if arr[0] == last_user_id.__str__():
            opponent = arr[1]
        create_object('Membership', {'AccountId': opponent,
                                     'ConversationId': id_,
                                     'ConversationType': type_,
                                     'LastVisit': datetime.now(),
                                     'IsAdmin': True})

    s = ''
    if type_ == 'G':
        s = 'group'
    elif type_ == 'C':
        s = 'channel'
    else:
        return
    post_systemic(type_, id_, s + ' created')


def send_message(phone_no, *message):
    global last_user_id
    message = ' '.join(message)

    receiver_id = get_object('Account', {'PhoneNumber': phone_no}, ['Id'])
    if not receiver_id:
        print("ID is not found")
    receiver_id = receiver_id[0]
    temp_arr = sorted([str(last_user_id), str(receiver_id)])
    temp_arr = [temp_arr[0]] + ['_'] + [temp_arr[1]]
    conversation_id = ''.join(temp_arr)
    create_conversation_if_necessary(conversation_id, 'D', conversation_id)
    create_object('Message', {'Text': message,
                              'Date': datetime.now(),
                              'SenderId': last_user_id,
                              'ConversationId': conversation_id,
                              'ConversationType': 'D'})
    update_object('Membership', {'ConversationId': conversation_id, 'AccountId': last_user_id, 'ConversationType': 'D'},
                  {'LastVisit': datetime.now()})


def create_channel(id_, name):
    create_conversation_if_necessary(id_, 'C', name)


def send_message_channel(id_, *message):
    global last_user_id
    message = ' '.join(message)

    membership = get_object('Membership',
                            {'AccountId': last_user_id, 'ConversationId': id_,
                             'ConversationType': 'C'}, ['IsAdmin'])

    if membership is None:
        print("ID is not found")
        return

    if not membership[0]:
        print("Access denied")
        return

    create_object('Message',
                  {'Text': message,
                   'SenderId': last_user_id,
                   'ConversationId': id_,
                   'ConversationType': 'C',
                   'Date': datetime.now()})

    update_object('Membership', {'ConversationId': id_, 'AccountId': last_user_id, 'ConversationType': 'C'},
                  {'LastVisit': datetime.now()})


def create_group(id_, name):
    create_conversation_if_necessary(id_, 'G', name)


def send_message_group(id_, *message):
    global last_user_id
    message = ' '.join(message)

    membership = get_object('Membership',
                            {'AccountId': last_user_id,
                             'ConversationId': id_,
                             'ConversationType': 'G'}, ['AccountId'])
    if membership is None:
        print("ID is not found")

    create_object('Message',
                  {'Text': message,
                   'SenderId': last_user_id,
                   'ConversationId': id_,
                   'ConversationType': 'G',
                   'Date': datetime.now()})

    update_object('Membership', {'ConversationId': id_, 'AccountId': last_user_id, 'ConversationType': 'G'},
                  {'LastVisit': datetime.now()})


def set_channel_link(id_, link):
    global last_user_id
    mem = get_object('Membership', {'ConversationId': id_,
                                    'ConversationType': 'C',
                                    'AccountId': last_user_id},
                     ['IsAdmin'])
    if not mem or not mem[0]:
        print('Access denied.')

    conversation_link = get_object('ConversationLink',
                                   {'ConversationId': id_,
                                    'ConversationType': 'C'},
                                   ['Link'])

    if conversation_link is None:
        create_object('ConversationLink',
                      {'ConversationId': id_, 'ConversationType': 'C',
                       'Link': link})
    else:
        update_object('ConversationLink',
                      {'ConversationId': id_,
                       'ConversationType': 'C'},
                      {'Link': link})


def set_group_link(id_, link):
    global last_user_id
    mem = get_object('Membership', {'ConversationId': id_,
                                    'ConversationType': 'G',
                                    'AccountId': last_user_id},
                     ['IsAdmin'])
    if not mem or not mem[0]:
        print('Access denied.')
    conversation_link = get_object('ConversationLink',
                                   {'ConversationId': id_,
                                    'ConversationType': 'G'},
                                   ['Link'])

    if conversation_link is None:
        create_object('ConversationLink',
                      {'ConversationId': id_, 'ConversationType': 'G',
                       'Link': link})
    else:
        update_object('ConversationLink',
                      {'ConversationId': id_,
                       'ConversationType': 'G'},
                      {'Link': link})


def join_channel(id_):
    global last_user_id
    if get_object('Conversation', {'Id': id_,
                                   'Type': 'C'}, ['Id']):
        create_object('Membership', {'AccountId': last_user_id,
                                     'ConversationId': id_,
                                     'ConversationType': 'C',
                                     'LastVisit': datetime.now(),
                                     'IsAdmin': False})
    else:
        print('No Such Channel.')


def join_link(link):
    global last_user_id
    id_ = get_object('ConversationLink', {'Link': link},
                     ['ConversationId', 'ConversationType'])
    if not id_:
        print('ID is not found')
    id_, type_ = id_[0], id_[1]
    create_object('Membership', {'AccountId': last_user_id,
                                 'ConversationId': id_,
                                 'ConversationType': type_,
                                 'LastVisit': datetime.now(),
                                 'IsAdmin': False})
    if type_ == 'G':
        a = getName(last_user_id)
        post_systemic('G', id_, a + ' joined group')


def leave_channel(id_):
    global last_user_id
    delete_object('Membership', {'ConversationId': id_,
                                 'AccountId': last_user_id,
                                 'ConversationType': 'C'})


def leave_group(id_):
    global last_user_id
    delete_object('Membership', {'ConversationId': id_,
                                 'AccountId': last_user_id,
                                 'ConversationType': 'G'})
    a = getName(last_user_id)
    post_systemic('G', id_, a + ' left group')


def home(do_print=True):
    global last_user_id
    conversationIds = filter_object('Membership', {'AccountId': last_user_id},
                                    ['ConversationId', 'LastVisit'])
    conversation = []
    for e in conversationIds:
        conv_id = e[0]
        messages = filter_object('Message', {'ConversationId': conv_id}, ['date'])
        messages.sort(key=lambda l: l[0], reverse=True)
        date = messages[0][0]
        unread = 0
        for x in messages:
            if x[0] > e[1]:
                unread += 1
        name, conv_type = get_object('Conversation', {'Id': conv_id}, ['Name', 'Type'])
        conversation += [(name, conv_type, date, unread)]
    conversation.sort(key=lambda l: l[2], reverse=True)
    if do_print:
        if len(conversation) <= 8:
            print_conv_table(conversation)
        else:
            print_conv_table(conversation[:8])
    return conversation


def find_who_the_hell_that_fucking_guy_was(_id):
    arr = _id.split('_')
    guy_f_id = arr[0]
    if arr[0] == last_user_id.__str__():
        guy_f_id = arr[1]
    x = get_object('account', {'Id': guy_f_id}, ['Name', 'PhoneNumber'])
    if x[0] is not None:
        return x[0]
    return x[1]


def print_chat_table(chat_list):
    for x in chat_list:
        print_chat_message(x)


def print_chat_message(chat):
    name = 'Me'
    if chat[2].__str__() != last_user_id.__str__():
        x = get_object('Account', {'id': chat[2]}, ['name', 'PhoneNumber'])
        if x[0] is not None:
            name = x[0]
        else:
            name = x[1]
    show = 'Sender :' + name + ' Time:\"' + chat[1].year.__str__() + '-' + chat[1].month.__str__() + '-' \
           + chat[1].day.__str__() + ' ' + chat[1].hour.__str__() + ':' + chat[1].minute.__str__() + ':' \
           + chat[1].second.__str__() + '\" Message:\"' + chat[0] + '\"'
    print(show)


def post_systemic(type_, conv_id, message):
    id_ = get_object('Account', {'Name': 'System',
                           'PhoneNumber': '00000000000'}, ['Id'])[0]
    create_object('Message', {'ConversationId': conv_id,
                              'ConversationType': type_,
                              'Text': message,
                              'SenderId': id_,
                              'Date': datetime.now()})


def print_conv_table(conv_list):
    for x in conv_list:
        print_row_conv(x)


def print_row_conv(x):
    name = x[0]
    show = 'chat with '
    if x[1] == 'D':
        name = find_who_the_hell_that_fucking_guy_was(name)
    elif x[1] == 'G':
        show = 'group '
    elif x[1] == 'C':
        show = 'channel '
    show += name
    print(show + ', unread_count=', x[3])


def view_chat(phone):
    opponent_id = get_object('Account', {'PhoneNumber': phone}, ['id'])[0]
    temp_arr = sorted([str(last_user_id), str(opponent_id)])
    temp_arr = [temp_arr[0]] + ['_'] + [temp_arr[1]]
    conversation_id = ''.join(temp_arr)
    view_channel(conversation_id, 'D')


def view_channel(conv_id, conv_type='C'):
    if get_object('Membership',
                  {'ConversationId': conv_id, 'AccountId': last_user_id, 'ConversationType': conv_type},
                  ['AccountId', 'ConversationId', 'ConversationType', 'LastVisit', 'IsAdmin']) is not None:
        update_object('Membership',
                      {'ConversationId': conv_id, 'AccountId': last_user_id, 'ConversationType': conv_type},
                      {'LastVisit': datetime.now()})
        messages = filter_object('Message', {'ConversationId': conv_id},
                                 ['Text', 'Date', 'SenderId', 'ConversationId',
                                  'ConversationType'])
        messages.sort(key=lambda l: l[1])
        if len(messages) >= 20:
            print_chat_table(messages[len(messages) - 20: len(messages)])
        else:
            print_chat_table(messages)


def view_group(_id):
    view_channel(_id, 'G')


def view_user_profile(phone):
    user = get_object('Account', {'PhoneNumber': phone}, ['Name', 'Id'])
    bio = get_object('AccountSetting', {'AccountId': user[1]}, ['Bio'])
    if user[0] is None:
        user = 'Name:NULL'
    else:
        user = 'Name:\"' + user[0] + '\"'
    if bio[0] is None:
        bio = 'Bio:NULL'
    else:
        bio = 'Bio:\"' + bio[0] + '\"'
    print(user + ' ' + bio)


def view_channel_profile(_id):
    global last_user_id
    role = get_object('Membership',
                      {'AccountId': last_user_id, 'ConversationId': _id,
                       'ConversationType': 'C'},
                      ['isAdmin'])
    if not role:
        print("Access denied.")
        return
    convName = get_object('Conversation', {'Id': _id, 'Type': 'C'},
                          ['Name'])[0]
    link = get_object('ConversationLink', {'ConversationId': _id, 'ConversationType': 'C'},
                      ['Link'])[0]
    members = filter_object('Membership', {'ConversationId': _id, 'ConversationType': 'C'}, ['accountId'])
    print('Name:\"' + convName + '\" MembersCount:' + len(members).__str__() + ' Link:\"' + link + '\"')

    if role[0] is True:
        print('Creator:\"' + getName(last_user_id) + '\"')
        accounts = filter_object('Membership', {'ConversationId': _id}, ['AccountId', 'isAdmin'])
        names = []
        for e in accounts:
                names += ['\"' + getName(e[0]) + '\"']
        print('Members:' + ' ,'.join(names))


def count_unread():
    conversations = home(False)
    unread = 0
    for e in conversations:
        unread += e[3]
    print(unread)


def view_group_profile(id_):
    group = get_object('Conversation', {'Id': id_, 'Type': 'G'}, ['Name'])
    if group is None:
        print("Id is not found")
        return
    name = group[0]

    conversation_link = get_object('ConversationLink',
                                   {'ConversationId': id_,
                                    'ConversationType': 'G'}, ['Link'])
    if conversation_link is not None:
        conversation_link = conversation_link[0]

    members_ids = filter_object('Membership',
                                {'ConversationId': id_,
                                 'ConversationType': 'G'}, ['AccountId'])
    members_count = len(members_ids)

    members = []
    for member_id in members_ids:
        member = get_object('Account', {'Id': member_id},
                            ['Name', 'PhoneNumber'])
        members.append(member[0] or member[1])
    members = ', '.join('\"%s\"' % member for member in members)

    creator = get_object('Membership',
                         {'ConversationId': id_, 'ConversationType': 'G', 'isAdmin': True},
                         ['AccountId'])
    if creator is None:
        raise Exception()

    creator = get_object('Account', {'Id': creator[0]}, ['Name', 'PhoneNumber'])
    creator = creator[0] or creator[1]

    if conversation_link:
        print(
            "Name:\"%s\" MembersCount:%d Link:\"%s\" Creator:\"%s\" Members:%s" % (
                name, members_count, conversation_link, creator, members))
    else:
        print(
            "Name:\"%s\" MembersCount:%d Creator:\"%s\" Members:%s" % (
                name, members_count, creator, members))


def getName(_id):
    x = get_object('account', {'id': _id}, ['name', 'phoneNumber'])
    if x[0] is not None:
        return x[0]
    return x[1]


def search_all(*words):
    global last_user_id
    messages = search_all_by_type(last_user_id, 'G', *words)
    messages += search_all_by_type(last_user_id, 'C', *words)
    messages += search_all_by_type(last_user_id, 'D', *words)
    messages.sort(key=lambda res: res[1], reverse=True)

    print_chat_table(messages)


def search_all_fuzzy(*words):
    global last_user_id
    messages = search_all_by_type_fuzzy(last_user_id, 'G', *words)
    messages += search_all_by_type_fuzzy(last_user_id, 'C', *words)
    messages += search_all_by_type_fuzzy(last_user_id, 'D', *words)
    messages.sort(key=lambda res: res[1], reverse=True)

    print_chat_table(messages)


def search_all_by_type(user_id, type, *words):
    groups = filter_object('Membership', {'AccountId': user_id,
                                          'ConversationType': type}, ['ConversationId'])

    if groups:
        query = {
            'bool': {
                'must': [
                    {
                        'bool': {
                            'must': [{"match": {"Text": word}} for word in words] +
                                    [{'match': {'ConversationType': type}}]
                        }
                    },
                    {
                        'bool': {
                            'should': [{'match': {'ConversationId': cid[0]}} for cid in groups]
                        }
                    }
                ]

            }
        }
        messages = elasticsearch.search(query)
        return messages
    return []


def search_all_by_type_fuzzy(user_id, type, *words):
    groups = filter_object('Membership', {'AccountId': user_id,
                                          'ConversationType': type}, ['ConversationId'])

    if groups:
        query = {
            'bool': {
                'must': [
                    {
                        'bool': {
                            'must': [{"fuzzy": {"Text": word}} for word in words] +
                                    [{'match': {'ConversationType': type}}]
                        }
                    },
                    {
                        'bool': {
                            'should': [{'match': {'ConversationId': cid[0]}} for cid in groups]
                        }
                    }
                ]

            }
        }
        messages = elasticsearch.search(query)
        return messages
    return []


def search_chat(phone_no, *words):
    receiver_id = get_object('Account', {'PhoneNumber': phone_no}, ['Id'])
    if not receiver_id:
        print("ID is not found")
        return

    temp_arr = sorted([str(last_user_id), str(receiver_id[0])])
    temp_arr = [temp_arr[0]] + ['_'] + [temp_arr[1]]
    conversation_id = ''.join(temp_arr)

    query = {
        'bool': {
            'must': [{'match': {"Text": word}} for word in words] +
                    [{'match': {'ConversationType': 'D'}},
                     {'match': {'ConversationId': conversation_id}}]
        }
    }

    messages = elasticsearch.search(query)
    print_chat_table(messages)


def search_group(group_id, *words):
    query = {
        'bool': {
            'must': [{'match': {"Text": word}} for word in words] +
                    [{'match': {'ConversationType': 'G'}},
                     {'match': {'ConversationId': group_id}}]
        }
    }

    messages = elasticsearch.search(query)
    print_chat_table(messages)


def search_channel(channel_id, *words):
    query = {
        'bool': {
            'must': [{'match': {"Text": word}} for word in words] +
                    [{'match': {'ConversationType': 'C'}},
                     {'match': {'ConversationId': channel_id}}]
        }
    }

    messages = elasticsearch.search(query)
    print_chat_table(messages)


def search_group_by_sender(group_id, phone_no, *words):
    receiver_id = get_object('Account', {'PhoneNumber': phone_no}, ['Id'])
    if not receiver_id:
        print("ID is not found")
        return

    query = {
        'bool': {
            'must': [{'match': {"Text": word}} for word in words] +
                    [{'match': {'ConversationType': 'G'}},
                     {'match': {'ConversationId': group_id}},
                     {'match': {'SenderId': receiver_id[0]}}]
        }
    }

    messages = elasticsearch.search(query)
    print_chat_table(messages)


def view_messages_by_date_chat(phone_no, chat_date):
    receiver_id = get_object('Account', {'PhoneNumber': phone_no}, ['Id'])
    if not receiver_id:
        print("ID is not found")
        return

    temp_arr = sorted([str(last_user_id), str(receiver_id[0])])
    temp_arr = [temp_arr[0]] + ['_'] + [temp_arr[1]]
    conversation_id = ''.join(temp_arr)

    query = {
        'bool': {
            'must': [{'match': {'ConversationType': 'D'}},
                     {'match': {'ConversationId': conversation_id}},
                     {'regexp': {'Date': '{}.*'.format(
                         chat_date.replace('-', '_'))}
                     }]
        }
    }

    messages = elasticsearch.search(query)
    print_chat_table(messages)


def view_messages_by_date_group(group_id, chat_date):
    query = {
        'bool': {
            'must': [{'match': {'ConversationType': 'G'}},
                     {'match': {'ConversationId': group_id}},
                     {'regexp': {'Date': '{}.*'.format(
                         chat_date.replace('-', '_'))}
                     }]
        }
    }

    messages = elasticsearch.search(query)
    print_chat_table(messages)


def view_messages_by_date_channel(channel_id, chat_date):
    query = {
        'bool': {
            'must': [{'match': {'ConversationType': 'C'}},
                     {'match': {'ConversationId': channel_id}},
                     {'regexp': {'Date': '{}.*'.format(
                         chat_date.replace('-', '_'))}
                     }]
        }
    }

    messages = elasticsearch.search(query)
    print_chat_table(messages)


def view_messages_by_date(chat_date):
    global last_user_id
    messages = view_all_messages_by_type(last_user_id, 'G', chat_date)
    messages += view_all_messages_by_type(last_user_id, 'C', chat_date)
    messages += view_all_messages_by_type(last_user_id, 'D', chat_date)
    messages.sort(key=lambda res: res[1], reverse=True)

    print_chat_table(messages)
    pass


def view_all_messages_by_type(user_id, type, chat_date):
    groups = filter_object('Membership', {'AccountId': user_id,
                                          'ConversationType': type}, ['ConversationId'])

    if groups:
        query = {
            'bool': {
                'must': [
                    {
                        'bool': {
                            'must': [{'match': {'ConversationType': type}},
                                     {'regexp': {'Date': '{}.*'.format(
                                         chat_date.replace('-', '_'))}
                                     }]
                        }
                    },
                    {
                        'bool': {
                            'should': [{'match': {'ConversationId': cid[0]}} for cid in groups]
                        }
                    }
                ]

            }
        }
        messages = elasticsearch.search(query)
        return messages
    return []
