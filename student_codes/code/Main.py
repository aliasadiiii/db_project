import db
import elasticsearch
import functions


def find_related_function_to_call(line):
    method_name, *args = line.split(' ')
    method = getattr(functions, method_name)
    method(*args)


def serve():
    db.connect()
    db.migrate()
    elasticsearch.flush()

    while True:
        try:
            line = input()
            find_related_function_to_call(line)
        except EOFError:
            break


if __name__ == '__main__':
    serve()
