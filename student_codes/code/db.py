import psycopg2

import configparser

_conn = None


def get_conn():
    global _conn
    return _conn


def connect():
    global _conn

    if _conn is not None:
        return
    config_parser = configparser.ConfigParser()
    config_parser.read('../../config.ini')

    config = config_parser['DEFAULT']

    host, port, user, password = (config['HOST'], config['PORT'],
                                  config['USER'], config['PASS'])
    _conn = psycopg2.connect(host=host, port=port, user=user, password=password)


def migrate():
    conn = get_conn()
    cur = conn.cursor()
    with open('../../database.sql', 'r') as f:
        cur.execute(f.read())
    conn.commit()
