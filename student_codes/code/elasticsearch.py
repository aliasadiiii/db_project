import configparser
import datetime
import json
import requests


def get_url():
    config_parser = configparser.ConfigParser()
    config_parser.read('../../config.ini')
    config = config_parser['DEFAULT']

    return 'http://{}:{}/message'.format(config['HOST'], config['ELASTIC_PORT'])


def flush():
    url = get_url()
    requests.delete(url)


def create_object(values):
    url = '{}/_doc'.format(get_url())
    values['Date'] = values['Date'].strftime('%Y_%m_%d %H:%M:%S.%f')

    requests.post(url=url, data=json.dumps(values), headers={
        'content-type': 'application/json'})


def search(data):
    url = '{}/_search'.format(get_url())
    data = {
        'query': data
    }

    response = requests.post(url=url, data=json.dumps(data), headers={
        'content-type': 'application/json'}).json()

    if response.get('error') is not None:
        print (response)
        return []

    result = [res['_source'] for res in response['hits']['hits']]
    messages = []

    for res in result:
        res['Date'] = datetime.datetime.strptime(res['Date'],
                                                 '%Y_%m_%d %H:%M:%S.%f')

    for res in result:
        messages.append(
            (res['Text'], res['Date'], res['SenderId'], res['ConversationId'],
             res['ConversationType'])
        )

    messages.sort(key=lambda res: res[1], reverse=True)
    return messages
