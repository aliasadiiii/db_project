SELECT COUNT(*) FROM (
  SELECT * FROM (
    SELECT * FROM ChannelMessage
    WHERE ChannelId IN (
      SELECT ChannelId FROM Channels
      WHERE AccountId = 123321
    )
  )
  JOIN Session On Date > Session.LastLoginDate
  WHERE Id = 123321
UNION
  SELECT * FROM (
    SELECT * FROM GroupMessage
    WHERE groupId IN (
      SELECT GroupId FROM Groups
      WHERE AccountId = 123321
    )
  )
  JOIN Session On Date > Session.LastLoginDate
  WHERE Id = 123321
UNION
  SELECT * FROM (
    SELECT * FROM Message
    WHERE Id IN (
      SELECT MessageId FROM PrivateMessage
      WHERE RecieverId = 123321
    )
  )
  JOIN Session On Date > Session.LastLoginDate
  WHERE Id = 123321
)