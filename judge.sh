#!/usr/bin/env bash

PATH_TO_STUDENT_CODE="student_codes/code"

pip3 install requests

lang=$(cat student_codes/config.ini | grep LANG | awk '{print $3}')
echo $lang
case "${lang}" in
"PYTHON")
    exec_command="python3 ${PATH_TO_STUDENT_CODE}/Main.py"
    ;;
"CPP")
    g++ ${PATH_TO_STUDENT_CODE}/Main.cpp -o ${PATH_TO_STUDENT_CODE}/Main_exec
    exec_command="${PATH_TO_STUDENT_CODE}/Main_exec"
    ;;
"JAVA")
    javac ${PATH_TO_STUDENT_CODE}e/Main.java
    exec_command="java ${PATH_TO_STUDENT_CODE}/Main"
    ;;
esac

if [ -d server_outputs ]; then
    rm -r server_outputs
fi

if [ -d student_outputs ]; then
    rm -r student_outputs
fi

mkdir server_outputs
mkdir student_outputs


total_counter=0
fail_counter=0
pass_counter=0

echo "----------------------------------------------------------------------"
for input_file in $(ls inputs); do
    file_name=$(echo ${input_file} | cut -d'.' -f 1)
    echo "contacting server on ${file_name}"
    python3 submitter.py inputs/${file_name}.in server_outputs/${file_name}.out
    cat inputs/${file_name}.in | eval ${exec_command} > student_outputs/${file_name}.out

    if [[ $(diff -sB student_outputs/${file_name}.out server_outputs/${file_name}.out) ]]; then
        pass_counter=$(( pass_counter+1 ))
        echo "test ${file_name} passed."
    else
        fail_counter=$(( fail_counter+1 ))
        echo "test ${file_name} failed."
    fi
    total_counter=$(( total_counter+1 ))
    echo "passed ${pass_counter}/${total_counter} tests | grade = $(( pass_counter/total_counter*100 ))"
    echo "----------------------------------------------------------------------"
done
