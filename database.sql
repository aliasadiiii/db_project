DROP TABLE IF EXISTS Account, Message, Conversation, ConversationLink, BlockedUser, AccountSetting, Session, Membership;


CREATE TABLE Account (
  Id          SERIAL PRIMARY KEY,
  Name        VARCHAR(50),
  PhoneNumber VARCHAR(12) UNIQUE
);

CREATE TABLE Conversation (
  Id   VARCHAR(50),
  Name VARCHAR(50),
  Type CHAR(1) CHECK (Type IN ('C', 'G', 'D')),
  PRIMARY KEY (Id, Type)
);

CREATE TABLE ConversationLink (
  ConversationId   VARCHAR(50),
  ConversationType VARCHAR(1),
  Link             VARCHAR(30),
  FOREIGN KEY (ConversationId, ConversationType) REFERENCES Conversation (Id, Type),
  PRIMARY KEY (ConversationId, ConversationType)
);

CREATE TABLE Message (
  Id               SERIAL PRIMARY KEY,
  Text             VARCHAR(100),
  Date             TIMESTAMP,
  SenderId         INT,
  ConversationId   VARCHAR(50),
  ConversationType VARCHAR(1),
  FOREIGN KEY (SenderId) REFERENCES Account (Id),
  FOREIGN KEY (ConversationId, ConversationType) REFERENCES Conversation (Id, Type)
);


CREATE TABLE BlockedUser (
  BlockedId INT,
  BlockerId INT,
  Date      TIMESTAMP,
  FOREIGN KEY (BlockedId) REFERENCES Account (Id)
);

CREATE TABLE AccountSetting (
  AccountId    INT PRIMARY KEY,
  Bio          VARCHAR(100),
  Password     VARCHAR(40),
  SelfDestruct INT CHECK (SelfDestruct IN (1, 2, 3, 4))
);


CREATE TABLE Session (
  Id            SERIAL PRIMARY KEY,
  AccountId     INT,
  CreatedDate   TIMESTAMP,
  LastLoginDate TIMESTAMP,
  DeviceName    INT,
  Ip            VARCHAR(15),
  FOREIGN KEY (AccountId) REFERENCES Account (Id)
);

CREATE TABLE Membership (
  AccountId        INT,
  ConversationId   VARCHAR(50),
  ConversationType VARCHAR(1),
  LastVisit        TIMESTAMP,
  IsAdmin          BOOLEAN,
  FOREIGN KEY (AccountId) REFERENCES Account (Id),
  FOREIGN KEY (ConversationId, ConversationType) REFERENCES Conversation (Id, Type),
  PRIMARY KEY (AccountId, ConversationId, ConversationType)
);
--
INSERT INTO Account (Name, PhoneNumber) VALUES ('System', '00000000000');
-- INSERT INTO AccountSetting (AccountId) VALUES (1);
-- INSERT INTO Conversation (Id, Name, Type, Link) VALUES ('1', 'khafana', 'G', 'khaar');
-- INSERT INTO Message (Text, Date, SenderId, ConversationId, ConversationType) VALUES ('salam salam', current_timestamp, 1, '1', 'G');
-- INSERT INTO Message (Text, Date, SenderId, ConversationId, ConversationType) VALUES ('hoy!', current_timestamp, 1, '1', 'G');
-- INSERT INTO Membership (AccountId, ConversationId, ConversationType, IsAdmin) VALUES (1, '1', 'G', TRUE);
