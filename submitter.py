import sys

import requests


def main():
    input_file_address = sys.argv[1]
    output_file_address = sys.argv[2]

    files = {'upload_file': open(input_file_address, 'rb')}
    r = requests.post('http://165.227.135.69/', files=files)

    with open(output_file_address, mode='w') as write_file:
        write_file.write(r.text)


if __name__ == '__main__':
    main()
